const express = require('express');
const connectDB = require('./config/db');
const cors = require('cors');
const bodyParser = require('body-parser');

//routes
const books = require('./routes/api/books');

const app = express();

connectDB();

app.use(cors({ origin: true, credentials: true }));
app.use(express.json({ extended: false }));
// app.use(bodyParser.json())
// app.use(bodyParser.urlencoded({ extended: false }))

app.use('/api/books', books);
const port = process.env.PORT || 8082;
app.listen(port, () => console.log(`Server running on port ${port}`));