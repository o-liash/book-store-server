const mongoose = require('mongoose');
const db_pass = require('../secrets/secrets');
const db = `mongodb+srv://oliash:${db_pass}@cluster0-eodxi.mongodb.net/test?retryWrites=true&w=majority`;

console.log(db);

const connectDB = async () => {
    try {
        await mongoose.connect(db, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        } 
        );

        console.log('MongoDB is Connected...');
    } catch(err) {
        console.error(err.message);
        process.exit(1);
    }
};

module.exports = connectDB;